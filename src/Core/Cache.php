<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Swallow\Core;

use Swallow\Cache\Redis;
use Swallow\Di\Di;

/**
 * 缓存类.
 *
 * @author     SpiritTeam
 *
 * @since      2015年1月9日
 *
 * @version    1.0
 */
class Cache
{
    /**
     * 缓存实例.
     *
     * @var \Swallow\Cache
     */
    private $cache;

    /**
     * 缓存数组配置.
     *
     * @var array
     */
    private $times;

    /**
     * 构造.
     */
    public function __construct()
    {
        $this->times = Conf::get('Swallow/cachestore');
        $this->cache = new Redis();
    }

    /**
     * 获取db类.
     *
     * @return self
     */
    public static function getInstance()
    {
        static $obj = null;
        if (isset($obj)) {
            return $obj;
        }
        $obj = new self();

        return $obj;
    }

    /**
     * 获取缓存的数据.
     *
     * @param string $key    缓存KEY
     * @param string $prefix 缓存KEY前缀
     *
     * @return mixed
     */
    public function get($key, $prefix = '')
    {
        try {
            return $this->cache->get($key);
        } catch (\Exception $e) {
            $this->cache->delete($key);

            return false;
        }
    }

    /**
     * 设置缓存.
     *
     * @param string $key      缓存KEY
     * @param mixed  $value    缓存的内容
     * @param string $lifetime 缓存时间或KEY前缀
     *
     * @return bool
     */
    public function set($key, $value, $lifetime = '')
    {
        $lifetime = $this->getLifetime($lifetime);

        return $this->cache->set($key, $value, $lifetime);
    }

    /**
     * 添加缓存.
     *
     * @param string $key      缓存KEY
     * @param mixed  $value    缓存的内容
     * @param string $lifetime 缓存时间或KEY前缀
     *
     * @return bool
     */
    public function add($key, $value, $lifetime = '')
    {
        $lifetime = $this->getLifetime($lifetime);

        return $this->cache->add($key, $value, $time);
    }

    /**
     * 删除缓存.
     *
     * @param string $key    缓存KEY
     * @param string $prefix 缓存KEY前缀
     *
     * @return bool
     */
    public function delete($key, $prefix = '')
    {
        return $this->cache->delete($key);
    }

    /**
     * 递增一个KEY值
     *
     * @param string $key
     * @param number $step   步进值
     * @param string $prefix 缓存KEY前缀
     *
     * @return bool
     */
    public function inc($key, $prefix = '', $step = 1)
    {
        return $this->cache->inc($key, $step);
    }

    /**
     * 递减一个KEY值
     *
     * @param string $key
     * @param number $step   步进值
     * @param string $prefix 缓存KEY前缀
     *
     * @return bool
     */
    public function dec($key, $prefix = '', $step = 1)
    {
        return $this->cache->dec($key, $step);
    }

    /**
     * 是否被强制清除.
     *
     * @return bool
     */
    public static function isSafeClear()
    {
        static $r = null;
        if (isset($r)) {
            return $r;
        }
        if ((isset($_GET['clear']) && 'cache' == $_GET['clear'] || 'cache' == Di::getInstance()->getShared('clientInfoNew')->getClearCache())
             && (DEBUG_MODE || $_ENV['isInternalUser'])) {
            $r = true;
        } else {
            $r = false;
        }

        return $r;
    }

    /**
     * @param $lifetime
     *
     * @return int
     */
    private function getLifetime($lifetime): int
    {
        if (!\is_int($lifetime)) {
            if (isset($this->times[$lifetime])) {
                $lifetime = $this->times[$lifetime]['ttl'];
            } else {
                $lifetime = 3600;
            }
        }

        return $lifetime;
    }
}
