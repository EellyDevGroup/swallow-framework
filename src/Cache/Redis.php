<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Swallow\Cache;

/**
 * Redis Cache驱动类.
 *
 * @author    陈淡华<chendanhua@eelly.net>
 *
 * @since     2016-1-9
 *
 * @version   1.0
 */
class Redis implements Cache
{
    /**
     * redis对象
     *
     * @var \Predis\Client
     */
    private $instance = null;

    /**
     * 初始化.
     */
    public function __construct()
    {
        $this->instance = \Swallow\Redis\Redis::getInstance();
    }

    /**
     * 关闭.
     */
    public function __destruct()
    {
        $this->instance->disconnect();
    }

    /**
     * 获取缓存的数据.
     *
     * @param string $key 缓存KEY
     *
     * @return mixed
     */
    public function get($key)
    {
        $value = $this->instance->get($key);
        if (null === $value) {
            return false;
        }
        return json_decode(gzuncompress($value), true);
    }

    /**
     * 设置缓存.
     *
     * @param string $key   缓存KEY
     * @param mixed  $value 缓存的内容
     * @param int    $time  缓存时间
     *
     * @return bool
     */
    public function set($key, $value, $time = 0)
    {
        $value = gzcompress(json_encode($value));
        if ($time > 0) {
            return $this->instance->setex($key, $time, $value);
        }

        return $this->instance->set($key, $value);
    }

    /**
     * 添加缓存.
     *
     * @param string $key   缓存KEY
     * @param mixed  $value 缓存的内容
     * @param int    $time  缓存时间
     *
     * @return bool
     */
    public function add($key, $value, $time = 0)
    {
        if ($this->instance->exists($key)) {
            return false;
        }

        return $this->set($key, $value, $time);
    }

    /**
     * 递增一个KEY值
     *
     * @param string $key
     * @param number $step 步进值
     *
     * @return bool
     */
    public function inc($key, $step = 1)
    {
        return $this->instance->incr($key, $step);
    }

    /**
     * 递减一个KEY值
     *
     * @param string $key
     * @param string $prefix 缓存KEY前缀
     * @param number $step   步进值
     *
     * @return bool
     */
    public function dec($key, $step = 1)
    {
        return $this->instance->decr($key, $step);
    }

    /**
     * 删除缓存.
     *
     * @param string $key 缓存KEY
     *
     * @return bool
     */
    public function delete($key)
    {
        $this->instance->del($key);
    }
}
