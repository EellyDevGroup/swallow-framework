<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Swallow\Cache;

/**
 * 静态缓存接口.
 *
 *
 * @author    hehui<hehui@eelly.net>
 *
 * @since     2016年10月7日
 *
 * @version   1.0
 */
interface StaticCacheInterface
{
    /**
     * 设置缓存.
     *
     *
     * @param string $key
     * @param mix    $value
     * @param int    $expired 过期时间(同memcached的ttl, -1 不过期)
     *
     * @return bool
     *
     * @author hehui<hehui@eelly.net>
     *
     * @since  2016年10月7日
     */
    public static function set($key, $value, $expired = -1);

    /**
     * 获取缓存数据.
     *
     * 返回null表示不存在或过期
     *
     * @param string $key
     *
     * @return array|null
     *
     * @author hehui<hehui@eelly.net>
     *
     * @since  2016年10月7日
     */
    public static function get($key);
}
